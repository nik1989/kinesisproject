import pymongo
import argparse

DESTMONGODB = 'mongodb://10.10.1.204:27017/'#'mongodb://localhost:27017/'
SRCMONGODB = 'mongodb://10.10.1.204:27017/'
SOCIALDB = pymongo.MongoClient(SRCMONGODB)['takenmate']
APPUSERCONTACTS = SOCIALDB.get_collection('appusercontacts')
APPUSERS = SOCIALDB.get_collection('appusers')
SOFTPROFILES = pymongo.MongoClient(DESTMONGODB)['takenmate'].get_collection('softprofile_v2')


def get_user_legit_id(appFBId):
    user = list(APPUSERS.find({"appFBId":appFBId},{"legit_id":1}))
    legit = user[0].get('legit_id','') if len(user) > 0 else ''
    return legit


def get_usercontacts(batchsize):
    firstpage = list(APPUSERCONTACTS.find({}).limit(1).sort("_id", 1))
    # firstpage= list(APPUSERCONTACTS.find({"_id":ObjectId("59d6e7d8552f797b6f095258")}).limit(1).sort("_id",1))
    firstuser = firstpage[0] if firstpage else None
    # print firstuser
    if firstuser:
        yield [firstuser]
        last_user = firstuser.get("_id")
        while True:
            nextpage = list(APPUSERCONTACTS.find({"_id": {"$gt": last_user}}).limit(batchsize).sort("_id", 1))
            yield [x for x in nextpage]
            if len(nextpage) > 0:
                last_user = nextpage[-1].get("_id")
                print("LAST CONTACT : ", last_user)
            else:
                return


def create_softProfile(contacts):
    docs = []
    try:
        for contact in contacts:
            pcontacts = contact["contactRow"]["phones"]
            for phone in pcontacts:
                if len(phone["data"]) >= 10:
                    c= dict()
                    if "fullName" in contact["contactRow"]:
                        c['contactName'] = contact["contactRow"]["fullName"]
                    else:
                        c['contactName'] = "NONE"
                    c['appFBId'] = contact['appFBId']
                    c['legit_id']=get_user_legit_id(contact['appFBId'])
                    docs.append(pymongo.UpdateOne({'phoneNumber':phone["data"]},{'$push':{'contactof':c}},upsert=True))
                    print("adding phone contact : ",c,phone["data"])
        SOFTPROFILES.bulk_write(docs)
    except Exception as e:
        print(e)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-mongourl", "--mongourl", dest="mongourl",
                        help="The mongodb to push to kinesis.", metavar="STREAM_NAME",
                        default="mongodb://10.0.3.234:27017/", )
    parser.add_argument("-mongodb", "--mongodb", dest="mongodb",
                        help="The data to push to kinesis.", metavar="STREAM_NAME", default="takenmate", )

    parser.add_argument("-collection", "--collection", dest="collection",
                        help="The data to push to kinesis.", metavar="STREAM_NAME", default="appusers", )

    parser.add_argument("-batchsize", "--batchsize", dest="batchsize",
                        help="Number of records to push to kinesis.", metavar="STREAM_NAME", default=200, )


    try:
        args = parser.parse_args()
    except Exception as e:
        print(e)

    #mongourl = args.mongourl
    #mongodb = args.mongodb
    collection = args.collection
    batchsize = int(args.batchsize)

    
    for contacts in get_usercontacts(200):
        create_softProfile(contacts)