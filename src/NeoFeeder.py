import pymongo
from py2neo.ogm import GraphObject, Property, Related, RelatedTo
from py2neo import Graph, Relationship, authenticate
from bson.objectid import ObjectId
from py2neo import Node, Relationship
import json
import sys, random, time, argparse
from boto import kinesis
import boto
import time


NEOHOST=None
NEODB=None
NEOGRAPH=None



MONGODB = None#'mongodb://10.10.1.60:27017/'
SOCIALDB = None#pymongo.MongoClient(MONGODB)['takenmate']
APPUSERS = None#SOCIALDB.get_collection('appusers')
APPUSERCONTACTS = None#SOCIALDB.get_collection('appusercontacts')



NEO_TRANSACTION_DELAY=10
KINESIS_READ_DELAY=0.2


USERS_ADDED=0
BATCH=None



from multiprocessing import Process, Pipe
import threading
import os
import json
import time
Free_Process={}



def set_mongo_info(mongourl,mongodb,collection):
    global MONGODB,SOCIALDB,APPUSERS,APPUSERCONTACTS
    MONGODB = mongourl
    SOCIALDB = mongodb
    APPUSERS = collection




def SetUpMongo(mongourl,mongodb,collection):
    global MONGODB,SOCIALDB,APPUSERS,APPUSERCONTACTS
    MONGODB = mongourl
    SOCIALDB = mongodb
    APPUSERS = collection
    SOCIALDB = pymongo.MongoClient(MONGODB)[SOCIALDB]
    APPUSERS = SOCIALDB.get_collection(APPUSERS)
    APPUSERCONTACTS = SOCIALDB.get_collection('appusercontacts')




def connect_to_neo4j(host,db,user,password):
    host_url = 'http://{}/{}'.format(host,db)
    authenticate(host,user,password)
    con = Graph(host_url, bolt=False)
    return con



class PhoneContact(GraphObject):
    __primarykey__ = "phoneNumber"
    phoneNumber = Property()
    primaryName = Property()
    contactName = Property()
    contactType = Property()
    contactOf = RelatedTo("UserNode")
    belongsTo = RelatedTo("UserNode", "user_phone_number")


class UserNode(GraphObject):
    __primarykey__ = "appFBId"
    appFBId = Property()
    fullName = Property()
    legitId = Property()
    hasContact = RelatedTo("UserNode","phone_contacts")
    belongsTo = RelatedTo("UserNode", "user_phone_number")
    fbFriends = RelatedTo("UserNode")
    legitifiFriends = RelatedTo("UserNode")
    email=Property()


# def create_user_node(user):
#     u = UserNode()
#     u.appFBId = user['appFBId']
#     u.fullName = user["contactRow"]["fullName"]
#     contact = PhoneContact()
#     #contact.phoneNumber = user['mobileNumber']
#     contact.contactName = user["contactRow"]["fullName"]
#     se = NEOGRAPH.begin()
#     se.merge(u)
#     se.merge(contact)
#     contact.belongsTo.add(u)
#     se.graph.push(contact)
#     se.commit()
#     return u


def get_or_create_usernode(usernode,neo_conn=None):  
    try:
        #query = '_.appFBId='+str(usernode['appFBId'])
        #data = list(UserNode.select(neo_conn).where(query))
        # if len(data)==1:
        #     return data[0]
        #else:
        u_node = UserNode()
        u_node.appFBId = usernode["appFBId"]

        transaction = neo_conn.begin()
        u_node.fullName = usernode['fullName']
        u_node.legitId=usernode['legitId']
        u_node.email=usernode['email']
        transaction.merge(u_node)
        transaction.graph.push(u_node)
        transaction.commit()
        return u_node
    except Exception as e:
        print("HERE...1   !!!!")
        print(e)
        return None


def get_or_create_user_phone_contact(usernode,mobile_number,neo_conn=None):
    
    try:
        #query = '_.phoneNumber='+str(mobile_number)
        #data = list(PhoneContact.select(neo_conn).where(query))
        #if len(data)==1:
        #    return data[0]
        #else:
        cdata=json.dumps({'contactName':usernode.fullName,'phoneNumber':mobile_number,'contactType':'phone'})
        Create_Contact(usernode,cdata,belongs_to=True,neo_conn=neo_conn)
    except Exception as e:
        print("HERE...2   !!!!")
        print(e)
        return None


def Create_Contacts(usernode,contacts,neo_conn=None):
    transaction = neo_conn.begin()
    for contact in contacts:
        pc = PhoneContact()
        contactdata = json.loads(contact)
        pc.phoneNumber = contactdata["phoneNumber"]
        pc.contactName = contactdata["contactName"]
        pc.contactType = contactdata["contactType"]
        transaction.merge(pc)
        usernode.hasContact.add(pc)
    transaction.graph.push(usernode)
    transaction.commit()
    print("CONTACTS PUSHED ",len(contacts)," for ",usernode.appFBId)


def Create_Contact(usernode,contactdata,belongs_to=False,neo_conn=None):
    transaction = neo_conn.begin()
    pc = PhoneContact()
    contactdata = json.loads(contactdata)
    pc.phoneNumber = contactdata["phoneNumber"]
    pc.contactName = contactdata["contactName"]
    pc.contactType = contactdata["contactType"]
    if belongs_to:
        #print("belongs to: ",usernode.fullName)
        pc.belongsTo.add(usernode)
    
    transaction.merge(pc)
    usernode.hasContact.add(pc)
    transaction.graph.push(usernode)


def get_user_appfbid(batchsize):
    # while True:
    #     yield [1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10]
    firstpage= list(APPUSERS.find({"countryCode":"IN"}).limit(1).sort("_id",1))
    #firstpage= list(APPUSERS.find({"_id":ObjectId("59438235637a12c806563761")}).limit(1).sort("_id",1))
    firstuser= firstpage[0] if firstpage else None
    #print firstuser
    if firstuser:
        yield [str(firstuser.get('legit_id'))]
        last_user = firstuser.get("_id")
        while True:
            nextpage = list(APPUSERS.find({"$and": [{"_id":{"$gt":last_user}},{"countryCode":"IN"}]}).limit(batchsize).sort("_id",1))
            yield [str(x.get('legit_id')) for x in nextpage]
            if len(nextpage) > 0:
                last_user=nextpage[-1].get("_id")
                print("LAST USER : ",last_user)
            else:
                return
def get_user_info(conn,userid):

    user = conn.find_one({"legit_id": userid})
    #print("user is ",user)
    return user


def create_user_node_dict(user):
    u = dict()
    u['appFBId'] = user['appFBId']
    u['fullName'] = user["fullName"]
    u['mobiles']=user['mobileNumber']
    u['email']=user.get('email',None)
    u['legitId']=user.get('legit_id',None)
    return u


def create_phone_contact_dict(u_node,conn):
    contacts = conn.find({'appFBId': u_node['appFBId']}, {"_id": 0, "contactRow": 1}, no_cursor_timeout=True)
    contact_nodes=[]
    tc = contacts.count()
    contact_count = 0
    ph = set()
    if contacts.count() > 0:
        for contact in contacts:
            pcontacts = contact["contactRow"]["phones"]
            for phone in pcontacts:
                #pprint.pprint("Creating Contact ",u_node.appFBId)
                if not phone["data"] in ph:
                    if len(phone["data"]) >= 10:
                        contact_count += 1
                        ph.add(phone["data"])
                        pc = dict()
                        if "fullName" in contact["contactRow"]:
                            pc['contactName'] = contact["contactRow"]["fullName"]
                        else:
                            pc['contactName'] = ""
                        pc['phoneNumber'] = phone["data"]
                        pc['contactType'] = phone["type"]
                        #print("adding phone contact : ",pc.contactName,pc.phoneNumber," for user : ",u_node.fullName)
                        contact_nodes.append(json.dumps(pc))
    contacts.close()
    return contact_nodes


def process_records(d,sleeptime=0,appuser_collection=None,phonecontacts_collection=None,neo_conn=None):
    for uid in d:
        u = get_user_info(appuser_collection,uid)
        if u:
            user = create_user_node_dict(u)
            contacts= create_phone_contact_dict(user,phonecontacts_collection)

            u_node=get_or_create_usernode(user,neo_conn=neo_conn)
            #print(u_node,u_node.fullName,u_node.legitId)
            if u_node:
                if len(user['mobiles']) > 0:
                    get_or_create_user_phone_contact(u_node,user['mobiles'],neo_conn=neo_conn)
                # for c in contacts:
                #     Create_Contact(u_node,c,neo_conn=neo_conn)
                Create_Contacts(u_node,contacts,neo_conn=neo_conn)
            time.sleep(sleeptime)

        else:
            print("NO user ",uid)


def runprocess(inchannel,process_key,sleeptime,jsondata,mongo_conn_data,neo_conn_data):
    global Free_Process


    mongo_conn=pymongo.MongoClient(mongo_conn_data['host'])[mongo_conn_data['db']]
    appuser_collection = mongo_conn.get_collection(mongo_conn_data['usercollections'])
    phonecontacts_collection = mongo_conn.get_collection(mongo_conn_data['phonecontacts'])

    neo_conn=connect_to_neo4j(neo_conn_data['host'],neo_conn_data['db'],neo_conn_data['user'],neo_conn_data['password'])
   
    print("NEO CONN ",neo_conn)
    while True:
        data = inchannel.recv()
        inchannel.send(False)
        #print("Got Data in ",os.getpid())
        if data=='stop':
            inchannel.send('stop')
            return
        if jsondata:
            data=json.loads(data)
        process_records(data,sleeptime=sleeptime,appuser_collection=appuser_collection,phonecontacts_collection=phonecontacts_collection,neo_conn=neo_conn)
        inchannel.send(True)
        


def from_child_process(pipeconn,pid):
    global Free_Process,USERS_ADDED
    while True:
        d=pipeconn.recv()
        if d=='stop':
            return
        else:
            if d:
                USERS_ADDED=USERS_ADDED+BATCH
                print("Number of users processed : ",USERS_ADDED)
            Free_Process[pid]=d



def create_pipe(pid,sleeptime=0,jsondata=False,mongo_conn_data={}):
    parent_conn, child_conn = Pipe()
    p = Process(target=runprocess, args=(child_conn,pid,sleeptime,jsondata,mongo_conn_data))
    return parent_conn, child_conn ,p

def parallelProcess(mongostream,num_processes=5,sleeptime=0,jsondata=False,mongo_conn_data={},neo_conn_data={}):
    processes=[]
    for pid in range(0,num_processes):
        parent_conn, child_conn = Pipe()
        p = Process(target=runprocess, args=(child_conn,pid,sleeptime,jsondata,mongo_conn_data,neo_conn_data))
        processes.append((p,parent_conn,child_conn))
        Free_Process[pid]=True
        threading.Thread(target=from_child_process,args=(parent_conn,pid)).start()
        p.start()
    count=0
    for data in mongostream:
        found=False
        count = count+len(data)
        while not found:
            for k,v in Free_Process.items():
                if v:
                    #print("sending to ",k)
                    if jsondata:
                        data=json.dumps(data)
                    processes[k][1].send(data)
                    found=True
                    break
            if not found:
                pass
                #print("Searching for free process")
        time.sleep(sleeptime)

    for i in range(0,len(processes)):
        processes[i][1].send('stop')
    
    for i in range(0,len(processes)):
        processes[i][0].join()



def test_stream():
    for i in range(0,100):
        yield 10*[i]

def setBatch(batch):
    global BATCH
    BATCH = batch

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-mongourl", "--mongourl", dest="mongourl",
                      help="The mongodb to push to kinesis.", metavar="STREAM_NAME",default="mongodb://10.0.3.234:27017/",)
    parser.add_argument("-mongodb", "--mongodb", dest="mongodb", 
                      help="The data to push to kinesis.", metavar="STREAM_NAME",default="takenmate",)

    parser.add_argument("-collection", "--collection", dest="collection", 
                      help="The data to push to kinesis.", metavar="STREAM_NAME",default="appusers",)
    
    parser.add_argument("-batchsize", "--batchsize", dest="batchsize", 
                      help="Number of records to push to kinesis.", metavar="STREAM_NAME",default=200,)

    parser.add_argument("-neo4jhost", "--neo4jhost", "--neo4jhost", dest="neohost", default="10.0.3.85:7474",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    parser.add_argument("-neo4jdb", "--neo4jdb", "--neo4jdb", dest="neodb", default="data/db",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    parser.add_argument("-neo4juser", "--neo4juser", "--neo4juser", dest="neo4juser", default="neo4j",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)
                    
    parser.add_argument("-neo4jpass", "--neo4jpass", "--neo4jpass", dest="neo4jpass", default="neo4j!123",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    parser.add_argument("-num_process", "--num_process", "--num_process", dest="num_process", default=1,
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    try:      
        args = parser.parse_args()
    except Exception as e:
        print(e)

    mongourl = args.mongourl
    mongodb = args.mongodb
    collection = args.collection
    batchsize = int(args.batchsize)

    setBatch(batchsize)

    neo4jhost=args.neohost
    neo4jdb=args.neodb
    neo4juser=args.neo4juser
    neo4jpass=args.neo4jpass

    num_process = int(args.num_process)


    SetUpMongo(mongourl,mongodb,collection)
    #connect_to_neo4j(neo4jhost,neo4jdb,neo4juser,neo4jpass)
    

    ug = get_user_appfbid(batchsize)#test_stream()

    mongo_conn_data = {'host':mongourl,'db':mongodb,'usercollections':collection,'phonecontacts':'appusercontacts'}
    neo_conn_data = {'host':neo4jhost,'db':neo4jdb,'user':neo4juser,'password':neo4jpass}
    parallelProcess(ug,num_processes=num_process,mongo_conn_data=mongo_conn_data,neo_conn_data=neo_conn_data)


