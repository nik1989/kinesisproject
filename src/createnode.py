
import pymongo
from py2neo.ogm import GraphObject, Property, Related, RelatedTo
from py2neo import Graph, Relationship, authenticate
from bson.objectid import ObjectId
import json
from PushToKinesis import execute
import os

ENVIRON = os.environ

REGION = ENVIRON.get('REGION','us-east-2')
DOWNSTREAM = ENVIRON.get('DOWNSTREAM','downstream')
MONGODB = ENVIRON.get('MONGOHOST','mongodb://18.217.19.149:27017/')
SOCIALDB = ENVIRON.get('MONGODB','socialgraph')
APPUSERS = ENVIRON.get('USERCOLLECTOIN','appusers')
APPUSERCONTACTS = ENVIRON.get('CONTACTSCOLLECTOIN','appusercontacts')


SOCIALDB = pymongo.MongoClient(MONGODB)[SOCIALDB]
APPUSERS = SOCIALDB.get_collection(APPUSERS)
APPUSERCONTACTS = SOCIALDB.get_collection(APPUSERCONTACTS)

# authenticate("18.217.19.149:7474", "neo4j", "nik@neo4j")
# NEO = 'http://18.217.19.149:7474/data/db'
# neograph = Graph(NEO, bolt=False)


# testIds = ["10154944004806305", ]


class PhoneContact(object):
    # __slots__ = ('phoneNumber','primaryName','contactName','contactType','contactOf','belongsTo')
    def __init__(self):
        self.phoneNumber = None
        self.primaryName = None
        self.contactName = None
        self.contactType = None
        self.contactOf = None
        self.belongsTo = None


    def to_json_string(self):
        data = {}
        return json.dumps(self.__dict__)


class UserNode(object):
    # __slots__ = ('appFBId','fullName','legitId','hasContact','belongsTo','fbFriends','legitifiFriends')
    def __init__(self):
        self.appFBId = None
        self.fullName = None
        self.legitId = None
        self.hasContact = []
        self.belongsTo = None
        self.fbFriends = []
        self.legitifiFriends = []

    def to_json_string(self):
        data = {}
        for k,v in self.__dict__.items():
            if type(v)==type([]):
                #print k
                ldata = []
                for x in v:
                    ldata.append(x.to_json_string())
                data[k]=ldata
            elif hasattr(v,'to_json_string'):
                data[k]=v.to_json_string()
            else:
                data[k]=v
        return data


def create_user_node(user):
    u = UserNode()
    u.appFBId = user['appFBId']
    u.fullName = user["contactRow"]["fullName"]
    return u


def create_phone_contact(u_node):
    contacts = APPUSERCONTACTS.find({'appFBId': u_node.appFBId}, {"_id": 0, "contactRow": 1}, no_cursor_timeout=True).batch_size(2000)
    contact_nodes=[]
    tc = contacts.count()
    contact_count = 0
    ph = set()
    if contacts.count() > 0:
        for contact in contacts:
            pcontacts = contact["contactRow"]["phones"]
            for phone in pcontacts:
                #pprint.pprint("Creating Contact ",u_node.appFBId)
                if not phone["data"] in ph:
                    contact_count += 1
                    ph.add(phone["data"])
                    pc = PhoneContact()
                    if "fullName" in contact["contactRow"]:
                        pc.contactName = contact["contactRow"]["fullName"]
                    else:
                        pc.contactName = ""
                    pc.phoneNumber = phone["data"]
                    pc.contactType = phone["type"]
                    u_node.hasContact.append(pc)
    contacts.close()
    return u_node


def get_user_info(userid):
    user = APPUSERS.find_one({"appFBId": userid})
    return user



def read_from_shard(shardid,kinesis):
    import base64
    import json
    shard_it = kinesis.get_shard_iterator(DOWNSTREAM, shardid, "TRIM_HORIZON")["ShardIterator"]
    while shard_it:
        res = kinesis.get_records(shard_it, limit=10)
        # print res['Records']
        for r in res['Records']:
            print(r)
            d = r['Data']
            # d = base64.decodestring(d)
            # d=json.loads(d)
            # for x in d['ids']:
            #     print x
                # contact = x['contact']
                # user = x['user']
                # print user
                # print contact
                # u_node=get_or_create_usernode(user)
                # print u_node
                # Create_Contact(u_node,contact)
        shard_it = res['NextShardIterator']

def start():
    from boto import kinesis
    import threading
    kinesis = kinesis.connect_to_region(REGION)
    description = kinesis.describe_stream('upstream')
    for sh in description['StreamDescription']['Shards']:
        threading.Thread(target=read_from_shard,args=(sh['ShardId'],kinesis)).start()


def lambda_handler(event, context):
    # TODO implement
    import base64
    import json
    for r in event['Records']:
        d = r['kinesis']['data']
        d = base64.b64decode(d).decode('utf-8')
        d=json.loads(d)
        downstreamdata = []
        for uid in d['ids']:
            u = get_user_info(uid)
            u_node = create_user_node(u)
            u_node= create_phone_contact(u_node)
            data = json.dumps(u_node.to_json_string())
            contacts = u_node.hasContact
            u_node.hasContact=[]
            for cnt in contacts:
                d = {'user':u_node.to_json_string(),'contact':cnt.to_json_string()}
                print(d)
                downstreamdata.append(d)
        execute('python','AppPhoneContactsProducer.py',REGION,DOWNSTREAM,downstreamdata)
        
