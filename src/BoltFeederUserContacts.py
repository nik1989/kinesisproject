import pymongo
from neo4j.v1 import GraphDatabase
import json
import sys, random, time, argparse
from bson.objectid import ObjectId



def get_user_phone_contact_node(driver):
    with driver.session() as session:
        with session.begin_transaction() as tx:
             for rec in tx.run("MATCH (n:UserNode) RETURN n.appFBId,n.fullName,n.mobile"):
                 yield rec

def create_single_user_phone_rel(rec,driver):
    with driver.session() as session:
        with session.begin_transaction() as tx:
                data = {'appfbid':rec['n.appFBId']}
                print(data)
                tx.run("Match (u:UserNode{appFBId:{appfbid}}),(p:PhoneContact{appFBId:{appfbid}}) WHERE NOT (u.fullName = p.contactName) MERGE (u)-[:phone_contacts]->(p)",**data)


if __name__ == '__main__':
    uri = "bolt://localhost:7687"
    driver = GraphDatabase.driver(uri, auth=("neo4j", "neodev"))
    for rec in get_user_phone_contact_node(driver):
        create_single_user_phone_rel(rec,driver)