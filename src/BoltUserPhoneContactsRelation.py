
import pymongo
from neo4j.v1 import GraphDatabase
import json
import sys, random, time, argparse
from bson.objectid import ObjectId





def create_single_user_phone_rel(rec,driver):
    with driver.session() as session:
        with session.begin_transaction() as tx:
             if len(rec['n.mobile']) > 1:
                data = {'appfbid':rec['n.appFBId']}
                tx.run("Match (u:UserNode{appFBId:{appfbid}}),(p:PhoneContact{appFBId:{appfbid}})  create unique (u)-[:phone_contacts]->(p)",**data)



def get_user_phone_contact_node(driver):
    with driver.session() as session:
        with session.begin_transaction() as tx:
             for rec in tx.run("MATCH (n:UserNode) RETURN n.appFBId,n.fullName,n.mobile"):
                 yield rec


if __name__ == '__main__':
    uri = "bolt://10.0.3.94:7687"
    driver = GraphDatabase.driver(uri, auth=("neo4j", "neo4j!123"))
    count=0
    for rec in get_user_phone_contact_node(driver):
        create_single_user_phone_rel(rec,driver)
        count = count+1
        print(count)


