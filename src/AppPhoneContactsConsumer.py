
import pymongo
from py2neo.ogm import GraphObject, Property, Related, RelatedTo
from py2neo import Graph, Relationship, authenticate
from bson.objectid import ObjectId
from py2neo import Node, Relationship
import json
import sys, random, time, argparse
from boto import kinesis
import boto
import time



NEOHOST=None
NEODB=None
NEOGRAPH=None



NEO_TRANSACTION_DELAY=10
KINESIS_READ_DELAY=0.2


USERS_ADDED=0




def connect_to_neo4j(host,db,user,password):
    global NEOGRAPH
    host_url = 'http://{}/{}'.format(host,db)
    authenticate(host,user,password)
    NEOGRAPH = Graph(host_url, bolt=False)



class PhoneContact(GraphObject):
    __primarykey__ = "phoneNumber"
    phoneNumber = Property()
    primaryName = Property()
    contactName = Property()
    contactType = Property()
    contactOf = RelatedTo("UserNode")
    belongsTo = RelatedTo("UserNode", "user_phone_number")


class UserNode(GraphObject):
    __primarykey__ = "appFBId"
    appFBId = Property()
    fullName = Property()
    legitId = Property()
    hasContact = RelatedTo("UserNode","phone_contacts")
    belongsTo = RelatedTo("UserNode", "user_phone_number")
    fbFriends = RelatedTo("UserNode")
    legitifiFriends = RelatedTo("UserNode")
    email=Property()


def create_user_node(user):
    u = UserNode()
    u.appFBId = user['appFBId']
    u.fullName = user["contactRow"]["fullName"]
    contact = PhoneContact()
    #contact.phoneNumber = user['mobileNumber']
    contact.contactName = user["contactRow"]["fullName"]
    se = NEOGRAPH.begin()
    se.merge(u)
    se.merge(contact)
    contact.belongsTo.add(u)
    se.graph.push(contact)
    se.commit()
    return u


def get_or_create_usernode(usernode):  
    try:
        query = '_.appFBId='+str(usernode['appFBId'])
        data = list(UserNode.select(NEOGRAPH).where(query))
        if len(data)==1:
            return data[0]
        else:
            u_node = UserNode()
            u_node.fullName = usernode['fullName']
            u_node.appFBId = usernode["appFBId"]
            u_node.legitId=usernode['legitId']
            u_node.email=usernode['email']
            se = NEOGRAPH.begin()
            se.graph.push(u_node)
            return u_node
    except Exception as e:
        print(e)
        return None


def get_or_create_user_phone_contact(usernode,mobile_number):
    
    try:
        query = '_.phoneNumber='+str(mobile_number)
        data = list(PhoneContact.select(NEOGRAPH).where(query))
        if len(data)==1:
            return data[0]
        else:
            cdata=json.dumps({'contactName':usernode.fullName,'phoneNumber':mobile_number,'contactType':'phone'})
            Create_Contact(usernode,cdata,belongs_to=True)
    except Exception as e:
        print(e)
        return None


def Create_Contact(usernode,contactdata,belongs_to=False):
    se = NEOGRAPH.begin()
    pc = PhoneContact()
    contactdata = json.loads(contactdata)
    pc.contactName = contactdata["contactName"]
    pc.phoneNumber = contactdata["phoneNumber"]
    pc.contactType = contactdata["contactType"]
    if belongs_to:
        print("belongs to: ",usernode.fullName)
        pc.belongsTo.add(usernode)
    #print('Saving Contact')
    se.merge(pc)
    usernode.hasContact.add(pc)
    se.graph.push(usernode)




def create_phone_contact_single(u_node,contact_data):
    se = NEOGRAPH.begin()
    pcontacts = contact["contactRow"]["phones"]
    pc = PhoneContact()
    pc.contactName = contact_data['contactName']
    pc.contactType = contact_data['contactType']
    pc.phoneNumber = contact_data['phoneNumber']
    se.merge(pc)
    u_node.hasContact.add(pc)
    se.graph.push(u_node)
    se.commit()




def lambda_handler(event, context):
    # TODO implement
    import base64
    import json
    for r in event['Records']:
        d = r['kinesis']['data']
        d = base64.decodestring(d)
        d=json.loads(d)
        for x in d['ids']:
            contact = x['contact']
            user = x['user']
            #print user
            #print contact
            #u_node=get_or_create_usernode(user)
            ##print u_node
            #Create_Contact(u_node,contact)


def read_from_shard(shardid,region,stream_name,iterator_type,kinesis):
    import base64
    import json
    shard_it = kinesis.get_shard_iterator(stream_name, shardid,iterator_type)["ShardIterator"]
    last_sequence=None
    last_time_stamp=None
    count=0
    restart=False
    data=None
    print("Starting Thread for Shard : ",shardid," neo_timeout :",NEO_TRANSACTION_DELAY, "kineis_delay : ",KINESIS_READ_DELAY)
    while shard_it:
        try:
            res = kinesis.get_records(shard_it, limit=10)
            if restart:
                print('##################RESTARTED :  ',shardid,last_sequence)
                restart=False
            for r in res['Records']:
                data = r['Data']
                data=json.loads(data)
                user=json.loads(data['user'])
                u_node=get_or_create_usernode(user)
                if u_node:
                    count=count+1
                    print("CREATE_USER_COUNT : ",count,"  SHARD : ",shardid)
                    if len(user['mobiles']) > 0:
                        get_or_create_user_phone_contact(u_node,user['mobiles'])
                    contacts=data['contacts']
                    for c in contacts:
                        Create_Contact(u_node,c)
                time.sleep(NEO_TRANSACTION_DELAY)
                last_sequence = r['SequenceNumber']
                if not last_sequence:
                    print(r)
                last_time_stamp=r.get('ApproximateArrivalTimestamp',None)
            shard_it = res['NextShardIterator']
            time.sleep(KINESIS_READ_DELAY)
        except boto.kinesis.exceptions.ProvisionedThroughputExceededException:
            print('ProvisionedThroughputExceededException found. Sleeping for 2 minutes ','shardid:',shardid,' lastsequence: ',last_sequence,'lasttimestamp: ',last_time_stamp)
            time.sleep(2*60)
            print("Restarting shard : ",shardid," from sequence : ",last_sequence)
            restart=True
            if last_sequence:
                iter_response = kinesis.get_shard_iterator(stream_name=stream_name,shard_id=shardid,shard_iterator_type='AFTER_SEQUENCE_NUMBER',starting_sequence_number=str(last_sequence))
                shard_it = iter_response['ShardIterator']
        except boto.kinesis.exceptions.ExpiredIteratorException:
            print("Expired iterator : ",shardid)
            if last_sequence:
                iter_response = kinesis.get_shard_iterator(stream_name=stream_name,shard_id=shardid,shard_iterator_type='AFTER_SEQUENCE_NUMBER',starting_sequence_number=str(last_sequence))
                shard_it = iter_response['ShardIterator']
        except Exception as e:
            print("Fatal Error : ",e," shard : ",shardid ,"data : ",data, "sequence : ",last_sequence)
            if last_sequence:
                iter_response = kinesis.get_shard_iterator(stream_name=stream_name,shard_id=shardid,shard_iterator_type='AFTER_SEQUENCE_NUMBER',starting_sequence_number=str(last_sequence))
                shard_it = iter_response['ShardIterator']

def start():
    from boto import kinesis
    import threading
    kinesis = kinesis.connect_to_region("us-east-2")
    description = kinesis.describe_stream('downstream')
    for sh in description['StreamDescription']['Shards']:
        threading.Thread(target=read_from_shard,args=(sh['ShardId'],kinesis)).start()

def wait_for_stream(conn, stream_name):
    '''
    Wait for the provided stream to become active.

    :type conn: boto.kinesis.layer1.KinesisConnection
    :param conn: A connection to Amazon Kinesis

    :type stream_name: str
    :param stream_name: The name of a stream.
    '''
    SLEEP_TIME_SECONDS = 3
    status = get_stream_status(conn, stream_name)
    while status != 'ACTIVE':
        print('{stream_name} has status: {status}, sleeping for {secs} seconds'.format(
                stream_name = stream_name,
                status      = status,
                secs        = SLEEP_TIME_SECONDS))
        time.sleep(SLEEP_TIME_SECONDS) # sleep for 3 seconds
        status = get_stream_status(conn, stream_name)

def get_stream_status(conn, stream_name):
    '''
    Query this provided connection object for the provided stream's status.

    :type conn: boto.kinesis.layer1.KinesisConnection
    :param conn: A connection to Amazon Kinesis

    :type stream_name: str
    :param stream_name: The name of a stream.

    :rtype: str
    :return: The stream's status
    '''
    description=get_stream_description(conn,stream_name)
    return description.get('StreamStatus')

def get_stream_description(conn,stream_name):
    r= conn.describe_stream(stream_name)
    description = r.get('StreamDescription')
    return description


def get_shards(conn,stream_name):
        description = get_stream_description(conn,stream_name)
        return description.get('Shards')

def parallel(conn,region,stream_name,shards,iterator_type):
    import threading
    for sh in shards:
        threading.Thread(target=read_from_shard,args=(sh['ShardId'],region,stream_name,iterator_type,conn)).start()





def process_kinesis_data_in_lambda():
    import base64
    import json
    d = base64.b64decode(encodeddata).decode('utf-8')
    data=json.loads(d)
    user=data['user']
    u_node=get_or_create_usernode(user)
    if u_node:
        if len(user['mobiles']) > 0:
            get_or_create_user_phone_contact(u_node,user['mobiles'])
        contacts=data['contacts']
        for c in contacts:
            Create_Contact(u_node,c)

def get_region():
    os.environ.get('REGION','us-east-2')

def get_stream():
    os.environ.get('STREAM','downstream')


def get_neo_host():
    os.environ.get('NEOHOST','10.10.3.146:7474')

def get_neo_db():
    os.environ.get('NEODB','data/db')

def get_neo_user():
    os.environ.get('NEOUSER','neo4j')

def get_neo_password():
    os.environ.get('NEOUSER','neo4j!123')


def set_neo_delay(delay):
    global NEO_TRANSACTION_DELAY
    NEO_TRANSACTION_DELAY=int(delay)

def set_kinesis_delay(delay):
    global KINESIS_READ_DELAY
    KINESIS_READ_DELAY=int(delay)

def multi_lambda_handler(event, context):
    # TODO implement
    import random
    from boto import kinesis
    processes=[]
    last_shard=None
    connect_to_neo4j(get_neo_host(),get_neo_db(),get_neo_user(),get_neo_password())
    kinesisconn = kinesis.connect_to_region(region_name = get_region())
    try:
        status = get_stream_status(kinesisconn,DOWNSTREAM)
        if 'DELETING' == status:
            print('The stream: {s} is being deleted, please rerun the script.'.format(s=get_stream()))
            sys.exit(1)
        elif 'ACTIVE' != status:
            wait_for_stream(kinesisconn, get_stream())
    except:
        # We'll assume the stream didn't exist so we will try to create it with just one shard
        #conn.create_stream(stream_name, 1)
        wait_for_stream(kinesisconn, get_stream())
    shards=[x['ShardId'] for x  in get_shards(kinesisconn,get_stream())]
    for r in event['Records']:
        d = r['kinesis']['data']
        sid=shards[random.randint(0,len(shards))%len(shards)]
        process = Process(target=process_kinesis_data_in_lambda, args=(d,get_region(),get_stream(),sid,))
        processes.append(process)
    for process in processes:
        process.start()
    for process in processes:
        process.join()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--stream", dest="stream_name", required=True,
                      help="The stream you'd like to create.", metavar="STREAM_NAME",)
    parser.add_argument("-r", "--region", "--region", dest="region", default="us-east-1",
                      help="The region you'd like to make this stream in. Default is 'us-east-2'", metavar="REGION_NAME",)
    parser.add_argument("-iter", "--iter", "--iter", dest="iter", default="LATEST",
                      help="The iterator type from stream. Default is 'LATEST'", metavar="REGION_NAME",)
    parser.add_argument("-neo4jhost", "--neo4jhost", "--neo4jhost", dest="neohost", default=None,
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    parser.add_argument("-neo4jdb", "--neo4jdb", "--neo4jdb", dest="neodb", default=None,
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    parser.add_argument("-neo4juser", "--neo4juser", "--neo4juser", dest="neo4juser", default=None,
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)
                    
    parser.add_argument("-neo4jpass", "--neo4jpass", "--neo4jpass", dest="neo4jpass", default=None,
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    parser.add_argument("-neo_delay", "--neo_delay", "--neo_delay", dest="neo_delay", default=0,
                      help="The neo4j transaction delay", metavar="NEO_HOST_NAME",)
    parser.add_argument("-kinesis_delay", "--kinesis_delay", "--kinesis_delay", dest="kinesis_delay", default=0.2,
                      help="The neo4j transaction delay", metavar="NEO_HOST_NAME",)
    

           
    args = parser.parse_args()
    stream_name = args.stream_name
    region = args.region
    iter_type=args.iter

    
    neo4jhost=args.neohost
    neo4jdb=args.neodb
    neo4juser=args.neo4juser
    neo4jpass=args.neo4jpass

    

    set_neo_delay(args.neo_delay)
    set_kinesis_delay(args.kinesis_delay)


    connect_to_neo4j(neo4jhost,neo4jdb,neo4juser,neo4jpass)
    '''
    Getting a connection to Amazon Kinesis will require that you have your credentials available to
    one of the standard credentials providers.
    '''
    print("Connecting to stream: {s} in {r}".format(s=stream_name, r=args.region))
    connection = kinesis.connect_to_region(region_name = args.region)
    try:
        status = get_stream_status(connection,stream_name)
        if 'DELETING' == status:
            print('The stream: {s} is being deleted, please rerun the script.'.format(s=stream_name))
            sys.exit(1)
        elif 'ACTIVE' != status:
            wait_for_stream(connection, stream_name)
    except:
        # We'll assume the stream didn't exist so we will try to create it with just one shard
        #conn.create_stream(stream_name, 1)
        wait_for_stream(connection, stream_name)


    
    shards = get_shards(connection,stream_name)
    parallel(connection,args.region,args.stream_name,shards,iter_type)
