

import pymongo
from neo4j.v1 import GraphDatabase
import json
import sys, random, time, argparse
from bson.objectid import ObjectId
import re

MONGODB = None#'mongodb://10.10.1.60:27017/'
SOCIALDB = None#pymongo.MongoClient(MONGODB)['takenmate']
APPUSERS = None#SOCIALDB.get_collection('appusers')
APPUSERCONTACTS = None#SOCIALDB.get_collection('appusercontacts')



def getMongoUser(appFbId):
    user= list(APPUSERS.find({"appFBId":appFbId}))
    return user


def createusernode(appFBId,driver):
    for user in getMongoUser(appFBId):
        usr = dict()
        usr['appFBId'] = user['appFBId']
        usr['fullName'] = user["fullName"]
        usr['mobiles']=user['mobileNumber']
        usr['email']=user.get('email',None)
        usr['legitId']=user.get('legit_id',None)
        usr['objId']=str(user.get('_id',''))
        with driver.session() as session:
            with session.begin_transaction() as tx:
                tx.run("Create (u:UserNode {appFBId:{appFBId},fullName:{fullName},mobile:{mobiles},email:{email},legitId:{legitId},ObjId:{objId}});",**usr)
                print("CREATED USER : ",user['appFBId'])



def getClusterLeader(driver,knowhost,neouser,neopass,protocol='bolt'):
    if not driver:
        driver = GraphDatabase.driver(knowhost, auth=(neouser,neopass))
    with driver.session() as session:
        with session.begin_transaction() as tx:
            d=tx.run("call dbms.cluster.overview()")
            info = d.data()
            leader = list(filter(lambda x : x['role']=="LEADER",info))
            if len(leader) > 0:
                leader = leader[0]
                uri = list(filter(lambda x : re.match('.*'+protocol+'.*',x),leader['addresses']))
                return uri[0] if len(uri) > 0 else None



def SetUpMongo(mongourl,mongodb,collection):
    global MONGODB,SOCIALDB,APPUSERS,APPUSERCONTACTS
    MONGODB = mongourl
    SOCIALDB = mongodb
    APPUSERS = collection
    SOCIALDB = pymongo.MongoClient(MONGODB)[SOCIALDB]
    APPUSERS = SOCIALDB.get_collection(APPUSERS)
    APPUSERCONTACTS = SOCIALDB.get_collection('appusercontacts')



def get_usercontacts(appFBId):
    contacts= list(APPUSERCONTACTS.find({"appFBId":appFBId}))
    return contacts


def create_phone_contact_dict(contacts,driver):
    contact_nodes=[]
    ph = set()
    for contact in contacts:
        pcontacts = contact["contactRow"]["phones"]
        for phone in pcontacts:
            #pprint.pprint("Creating Contact ",u_node.appFBId)
            if not phone["data"] in ph:
                if len(phone["data"]) >= 10:
                    ph.add(phone["data"])
                    pc = dict()
                    if "fullName" in contact["contactRow"]:
                        pc['contactName'] = contact["contactRow"]["fullName"]
                    else:
                        pc['contactName'] = ""
                    pc['phoneNumber'] = phone["data"]
                    pc['contactType'] = phone["type"]
                    pc['appFBId']=contact['appFBId']
                    with driver.session() as session:
                        with session.begin_transaction() as tx:
                            tx.run("Create (u:PhoneContact {contactName:{contactName},phoneNumber:{phoneNumber},contactType:{contactType},appFBId:{appFBId}});",**pc)
                            


def create_single_user_phone_rel(appFBId,driver):
    with driver.session() as session:
        with session.begin_transaction() as tx:
            data={'appfbid':appFBId}
            tx.run("Match (u:UserNode{appFBId:{appfbid}}),(p:PhoneContact{appFBId:{appfbid}})  create unique (u)-[:phone_contacts]->(p)",**data)


def Create_same_Contact(appFBId,driver):
    with driver.session() as session:
        with session.begin_transaction() as tx:
            data = {'appFBId':appFBId}
            tx.run("MATCH (n:UserNode{appFBId:{appFBId}})-[:phone_contacts]->(p:PhoneContact) with p match(p2:PhoneContact{phoneNumber:p.phoneNumber})  where not p=p2 merge (p)-[:same_contact]-(p2)",**data)




def UploadUser(appFBId,driver):
    createusernode(appFBId,driver)
    contacts=get_usercontacts(appFBId)
    create_phone_contact_dict(contacts,driver)
    create_single_user_phone_rel(appFBId,driver)
    Create_same_Contact(appFBId,driver)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-mongourl", "--mongourl", dest="mongourl",
                      help="The mongodb db url.", metavar="STREAM_NAME",default="mongodb://10.0.3.234:27017/",)
    parser.add_argument("-mongodb", "--mongodb", dest="mongodb", 
                      help="The data to push to kinesis.", metavar="STREAM_NAME",default="takenmate",)

    parser.add_argument("-collection", "--collection", dest="collection", 
                      help="The data to push to kinesis.", metavar="STREAM_NAME",default="appusers",)
    
    parser.add_argument("-batchsize", "--batchsize", dest="batchsize", 
                      help="Number of records to push to kinesis.", metavar="STREAM_NAME",default=200,)

    parser.add_argument("-neo4jhost", "--neo4jhost", "--neo4jhost", dest="neohost", default="",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    parser.add_argument("-neo4jdb", "--neo4jdb", "--neo4jdb", dest="neodb", default="data/db",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    parser.add_argument("-neo4juser", "--neo4juser", "--neo4juser", dest="neo4juser", default="",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)
                    
    parser.add_argument("-neo4jpass", "--neo4jpass", "--neo4jpass", dest="neo4jpass", default="neodev",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    parser.add_argument("-appFBId", "--appFBId", "--appFBId", dest="appFBId", default="",
                      help="user appfbid to fetch", metavar="appFBId",)

    try:      
        args = parser.parse_args()
    except Exception as e:
        print(e)

    mongourl = args.mongourl
    mongodb = args.mongodb
    collection = args.collection
    batchsize = int(args.batchsize)

    #setBatch(batchsize)

    neo4jhost=args.neohost
    neo4jdb=args.neodb
    neo4juser=args.neo4juser
    neo4jpass=args.neo4jpass

    if re.match(",.*",args.appFBId):
        appFBId=args.appFBId.split(",")[1]
    else:
        appFBId = args.appFBId

    SetUpMongo(mongourl,mongodb,collection)
    uri = neo4jhost#"bolt://localhost:7687"
    driver = GraphDatabase.driver(uri, auth=(neo4juser,neo4jpass))
    complete = False
    tries = 5
    while not complete and tries > 0:
        try:
            UploadUser(appFBId,driver)
            complete = True
            print("User Contacts updated ",uri)
        except Exception as e:
            leader = getClusterLeader(driver,uri,neo4juser,neo4jpass)
            uri = leader if leader else uri
            driver = GraphDatabase.driver(uri, auth=(neo4juser,neo4jpass))
            tries = tries - 1

        