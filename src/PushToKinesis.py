#!env python


from __future__ import print_function
import sys, random, time, argparse
from boto import kinesis
import json
import subprocess
from AppPhoneContactsProducer import run,kinesisconn

STREAM_DESCIPTION=None

def get_stream_description(conn,stream_name):
    global STREAM_DESCIPTION
    if not STREAM_DESCIPTION:
        r= conn.describe_stream(stream_name)
        STREAM_DESCIPTION = r.get('StreamDescription')
    return STREAM_DESCIPTION


def get_shards(conn,stream_name):
    global STREAM_DESCIPTION
    if not STREAM_DESCIPTION:
        get_stream_description(conn,stream_name)
    return STREAM_DESCIPTION.get('Shards')

def get_stream_status(conn, stream_name):
    global STREAM_DESCIPTION
    '''
    Query this provided connection object for the provided stream's status.

    :type conn: boto.kinesis.layer1.KinesisConnection
    :param conn: A connection to Amazon Kinesis

    :type stream_name: str
    :param stream_name: The name of a stream.

    :rtype: str
    :return: The stream's status
    '''
    if not STREAM_DESCIPTION:
        get_stream_description(conn,stream_name)
    return STREAM_DESCIPTION.get('StreamStatus')

def wait_for_stream(conn, stream_name):
    '''
    Wait for the provided stream to become active.

    :type conn: boto.kinesis.layer1.KinesisConnection
    :param conn: A connection to Amazon Kinesis

    :type stream_name: str
    :param stream_name: The name of a stream.
    '''
    SLEEP_TIME_SECONDS = 3
    status = get_stream_status(conn, stream_name)
    while status != 'ACTIVE':
        print('{stream_name} has status: {status}, sleeping for {secs} seconds'.format(
                stream_name = stream_name,
                status      = status,
                secs        = SLEEP_TIME_SECONDS))
        time.sleep(SLEEP_TIME_SECONDS) # sleep for 3 seconds
        status = get_stream_status(conn, stream_name)




def read_data_file(fpath):
    with open(fpath,'r') as f:
        data = json.load(f)
        return data



def getchunksofdata(datalist,chunksize):
    offset = 0
    stepsize = chunksize

    if chunksize == 0:
        return [datalist]

    chunks = []
    for i in range(0,len(datalist)/stepsize):
        chunk =  datalist[offset:offset+stepsize]
        offset = offset+stepsize
        chunks.append(chunk)
    
    if offset < len(datalist):
        final_chunk = datalist[offset:len(datalist)]
        chunks.append(final_chunk)
    
    return chunks




def execute(venv,runpy,region,stream_name,datalist):
    conn = kinesis.connect_to_region(region_name = region)
    try:
        status = get_stream_status(conn, stream_name)
        if 'DELETING' == status:
            print('The stream: {s} is being deleted, please rerun the script.'.format(s=stream_name))
            sys.exit(1)
        elif 'ACTIVE' != status:
            wait_for_stream(conn, stream_name)
    except:
        # We'll assume the stream didn't exist so we will try to create it with just one shard
        #conn.create_stream(stream_name, 1)
        wait_for_stream(conn, stream_name)
    
    
    
    data = datalist
    shards = get_shards(conn,stream_name)

    chunksize = len(data)/len(shards)
    if chunksize < 10:
        chunks = [data]
    else:
        chunks = getchunksofdata(data,len(data)/len(shards))
    
    kinesisconn=conn
    for i in range(0,len(chunks)):
        shard_id = shards[i]['ShardId']
        # subprocess.call([venv,runpy,region,stream_name,shard_id,json.dumps(chunks[i])])
        run(region,stream_name,shard_id,chunks[i])

if __name__ == '__main__':
    parser = argparse.ArgumentParser('''
Puts words into a stream.

# Using the -w option multiple times
kinesis_producer.py -s STREAM_NAME -data <jsonfilepath>

''')
    parser.add_argument("-s", "--stream", dest="stream_name", required=True,
                      help="The stream you'd like to create.", metavar="STREAM_NAME",)
    parser.add_argument("-r", "--regionName", "--region", dest="region", default="us-east-1",
                      help="The region you'd like to make this stream in. Default is 'us-east-1'", metavar="REGION_NAME",)
    parser.add_argument("-d", "--data", dest="datafile", required=True,
                      help="The data to push to kinesis.", metavar="STREAM_NAME",)
    parser.add_argument("-venv", "--venv", dest="venv", required=True,
                      help="The data to push to kinesis.", metavar="STREAM_NAME",)

    parser.add_argument("-runpy", "--runpy", dest="runpy", required=True,
                      help="The data to push to kinesis.", metavar="STREAM_NAME",)

                
    args = parser.parse_args()
    stream_name = args.stream_name

    '''
    Getting a connection to Amazon Kinesis will require that you have your credentials available to
    one of the standard credentials providers.
    '''
    print("Connecting to stream: {s} in {r}".format(s=stream_name, r=args.region))
    conn = kinesis.connect_to_region(region_name = args.region)
    try:
        status = get_stream_status(conn, stream_name)
        if 'DELETING' == status:
            print('The stream: {s} is being deleted, please rerun the script.'.format(s=stream_name))
            sys.exit(1)
        elif 'ACTIVE' != status:
            wait_for_stream(conn, stream_name)
    except:
        # We'll assume the stream didn't exist so we will try to create it with just one shard
        #conn.create_stream(stream_name, 1)
        wait_for_stream(conn, stream_name)
    
    
    datafile = args.datafile
    data = read_data_file(datafile)
    shards = get_shards(conn,stream_name)

    chunksize = len(data)/len(shards)
    if chunksize < 10:
        chunks = [data]
    else:
        chunks = getchunksofdata(data,len(data)/len(shards))
    
    for i in range(0,len(chunks)):
        shard_id = shards[i]['ShardId']
        subprocess.call([args.venv,args.runpy,args.region,stream_name,shard_id,json.dumps(chunks[i])])
        #subprocess.call(['/Volumes/MySpace/aws/env/bin/python','simplesubprocess.py','us-east-2','userphonesstream','shardId-000000000000',json.dumps([1,2,3,4,5])])
    

