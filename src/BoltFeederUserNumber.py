
import pymongo
from neo4j.v1 import GraphDatabase
import json
import sys, random, time, argparse
from bson.objectid import ObjectId




def get_user_phone_contact_node(driver):
    with driver.session() as session:
        with session.begin_transaction() as tx:
             for rec in tx.run("MATCH (n:UserNode) RETURN n.appFBId,n.fullName,n.mobile"):
                 yield rec


def Create_User_Contact(rec,driver):
    with driver.session() as session:
        with session.begin_transaction() as tx:
             if len(rec['n.mobile']) > 1:
                data = {'appFBId':rec['n.appFBId'],'mobile':rec['n.mobile']}
                tx.run("match (u:UserNode{appFBId:{appFBId}}),(p:PhoneContact{phoneNumber:{mobile}}) create  (u)-[:user_contact]->(p)",**data)


if __name__ == '__main__':
    uri = "bolt://10.0.3.94:7687"
    driver = GraphDatabase.driver(uri, auth=("neo4j", "neo4j!123"))
    for rec in get_user_phone_contact_node(driver):
        Create_User_Contact(rec,driver)


