import pymongo
import json
import multiprocessing


import sys, random, time, argparse
from boto import kinesis
import json
import subprocess
import concurrent.futures
from multiprocessing import Process, Pipe
import os


STREAM_DESCIPTION=None
KINESIS_CONN=None

MONGODB = 'mongodb://18.217.19.149:27017/'
SOCIALDB = pymongo.MongoClient(MONGODB)['socialgraph']
APPUSERS = SOCIALDB.get_collection('appusers')
APPUSERCONTACTS = SOCIALDB.get_collection('appusercontacts')


def wait_for_stream(conn, stream_name):
    '''
    Wait for the provided stream to become active.

    :type conn: boto.kinesis.layer1.KinesisConnection
    :param conn: A connection to Amazon Kinesis

    :type stream_name: str
    :param stream_name: The name of a stream.
    '''
    SLEEP_TIME_SECONDS = 3
    status = get_stream_status(conn, stream_name)
    while status != 'ACTIVE':
        print('{stream_name} has status: {status}, sleeping for {secs} seconds'.format(
                stream_name = stream_name,
                status      = status,
                secs        = SLEEP_TIME_SECONDS))
        time.sleep(SLEEP_TIME_SECONDS) # sleep for 3 seconds
        status = get_stream_status(conn, stream_name)

def get_stream_status(conn, stream_name):
    '''
    Query this provided connection object for the provided stream's status.

    :type conn: boto.kinesis.layer1.KinesisConnection
    :param conn: A connection to Amazon Kinesis

    :type stream_name: str
    :param stream_name: The name of a stream.

    :rtype: str
    :return: The stream's status
    '''
    description=get_stream_description(conn,stream_name)
    return description.get('StreamStatus')

def get_stream_description(conn,stream_name):
    r= conn.describe_stream(stream_name)
    description = r.get('StreamDescription')
    return description


def get_shards(conn,stream_name):
        description = get_stream_description(conn,stream_name)
        return description.get('Shards')


def get_user_appfbid(batchsize):
    # while True:
    #     yield [1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10]
    firstpage= list(APPUSERS.find({}).limit(1).sort("_id",1))
    firstuser= firstpage[0] if firstpage else None
    #print firstuser
    if firstuser:
        yield [str(firstuser.get('legit_id'))]
        last_user = firstuser.get("_id")
        while True:
            nextpage = list(APPUSERS.find({"_id":{"$gt":last_user}}).limit(batchsize).sort("_id",1))
            yield [str(x.get('legit_id')) for x in nextpage]
            if len(nextpage) > 0:
                last_user=nextpage[-1].get("_id")
            else:
                return

def run(conn,region,streamname,shardid,datalist):
    data = {'ids':datalist}
    #print(data,shardid)
    done=False
    while not done:
        try:
            d= conn.put_record(streamname, json.dumps(data), shardid)
            done=True
        except kinesis.exceptions.ProvisionedThroughputExceededException:
            print('ProvisionedThroughputExceededException found. Sleeping for 0.5 seconds...')
            time.sleep(0.5)
        except Exception as e:
            print(e)

def get_chunks(wholelist,num_of_shards):
    if len(wholelist) < num_of_shards:
        return [wholelist]
    remainder = len(wholelist)%num_of_shards
    num_of_item_in_chunks = int(len(wholelist)/num_of_shards)
    off_set=0
    chunks=[]
    for i in range(0,int((len(wholelist)-remainder)/num_of_item_in_chunks)):
        chunk = wholelist[off_set:off_set+num_of_item_in_chunks]
        off_set=off_set+num_of_item_in_chunks
        chunks.append(chunk)
    if remainder > 0:
        chunks.append(wholelist[-1*remainder:])
    return chunks

def runprocess(inchannel,region,streamname,shardid):
    try:
        conn = kinesis.connect_to_region(region_name = region)
    except Exception as e:
        print("Could not connect to kinesis region : ",region)
        return
    if not conn:
        print("con is none ",region)
        print(kinesis)
        return
    while True:
        done=False
        data = inchannel.recv()
        print(" process : ",os.getpid())
        if data=='stop':
            return
        while not done:
            done=True
            try:
                d= conn.put_record(streamname, json.dumps(data), shardid)
                print("sent ",data,done)
            except kinesis.exceptions.ProvisionedThroughputExceededException:
                print('ProvisionedThroughputExceededException found. Sleeping for 0.5 seconds...')
                done=False
                time.sleep(0.5)
            except Exception as e:
                done=False
                print(e)

def parallelProcess(conn,mongostream,region,stream_name,shards):
    processes=[]
    for s in shards:
        parent_conn, child_conn = Pipe()
        p = Process(target=runprocess, args=(child_conn,region,stream_name,s['ShardId']))
        processes.append((p,parent_conn))
        p.start()
    nextprocess=0
    for x in mongostream:
        print(x)
        chunks=get_chunks(x,len(shards))
        processes[nextprocess][1].send(x)
        nextprocess=(nextprocess+1)%len(processes)
        time.sleep(60)

    for i in range(0,len(processes)):
        processes[i][1].send('stop')
    
    for i in range(0,len(processes)):
        processes[i][0].join()

    


def parallel(conn,mongostream,region,stream_name,shards):
    count=1
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(shards)) as executor:
        for x in zip(*mongostream):
            for y in x:
                executor.submit(run,conn,region,stream_name,shards[count%len(shards)]['ShardId'],y)
                count=count+1



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--stream", dest="stream_name", required=True,
                      help="The stream you'd like to create.", metavar="STREAM_NAME",)
    parser.add_argument("-r", "--region", "--region", dest="region", default="us-east-1",
                      help="The region you'd like to make this stream in. Default is 'us-east-1'", metavar="REGION_NAME",)
    parser.add_argument("-mongourl", "--mongourl", dest="mongourl", required=True,
                      help="The mongodb to push to kinesis.", metavar="STREAM_NAME",)
    parser.add_argument("-mongodb", "--mongodb", dest="mongodb", required=True,
                      help="The data to push to kinesis.", metavar="STREAM_NAME",)

    parser.add_argument("-collection", "--collection", dest="collection", required=True,
                      help="The data to push to kinesis.", metavar="STREAM_NAME",)
    
    parser.add_argument("-batchsize", "--batchsize", dest="batchsize", required=True,
                      help="Number of records to push to kinesis.", metavar="STREAM_NAME",)

           
    args = parser.parse_args()
    stream_name = args.stream_name
    region = args.region
    mongourl = args.mongourl
    mongodb = args.mongodb
    collection = args.collection
    batchsize = int(args.batchsize)

    '''
    Getting a connection to Amazon Kinesis will require that you have your credentials available to
    one of the standard credentials providers.
    '''
    print("Connecting to stream: {s} in {r}".format(s=stream_name, r=args.region))
    connection = kinesis.connect_to_region(region_name = args.region)
    try:
        status = get_stream_status(connection,stream_name)
        if 'DELETING' == status:
            print('The stream: {s} is being deleted, please rerun the script.'.format(s=stream_name))
            sys.exit(1)
        elif 'ACTIVE' != status:
            wait_for_stream(connection, stream_name)
    except:
        # We'll assume the stream didn't exist so we will try to create it with just one shard
        #conn.create_stream(stream_name, 1)
        wait_for_stream(connection, stream_name)


    MONGODB = mongourl
    SOCIALDB = mongodb
    APPUSERS = collection
    SOCIALDB = pymongo.MongoClient(MONGODB)[SOCIALDB]
    APPUSERS = SOCIALDB.get_collection(APPUSERS)


    
    shards = get_shards(connection,stream_name)
    

    ug = get_user_appfbid(batchsize)
    parallelProcess(connection,ug,args.region,args.stream_name,shards)




    


