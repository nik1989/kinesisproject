
import pymongo
from py2neo.ogm import GraphObject, Property, Related, RelatedTo
from py2neo import Graph, Relationship, authenticate
from bson.objectid import ObjectId
import json
# from PushToKinesis import execute
import os
from multiprocessing import Process
from boto import kinesis
import time
import argparse
import boto
from random import *
from multiprocessing import Process, Pipe


ENVIRON = os.environ

# REGION = ENVIRON.get('REGION','us-east-2')
# DOWNSTREAM = ENVIRON.get('DOWNSTREAM','downstream')
# MONGODB = ENVIRON.get('MONGOHOST','mongodb://10.10.1.60:27017/')
# SOCIALDB = ENVIRON.get('MONGODB','takemate')
# APPUSERS = ENVIRON.get('USERCOLLECTOIN','appusers')
# APPUSERCONTACTS = ENVIRON.get('CONTACTSCOLLECTOIN','appusercontacts')


STREAM_DESCIPTION=None
KINESIS_CONN=None

MONGODB = None#'mongodb://10.10.1.60:27017/'
SOCIALDB = None#pymongo.MongoClient(MONGODB)['takenmate']
APPUSERS = None#SOCIALDB.get_collection('appusers')
APPUSERCONTACTS = None#SOCIALDB.get_collection('appusercontacts')




DOWN_STREAM_PROCESS_MAP=[]
LAST_DOWNSTREAM_PROCESS=None


MONGO_READ_DELAY=0


def wait_for_stream(conn, stream_name):
    '''
    Wait for the provided stream to become active.

    :type conn: boto.kinesis.layer1.KinesisConnection
    :param conn: A connection to Amazon Kinesis

    :type stream_name: str
    :param stream_name: The name of a stream.
    '''
    SLEEP_TIME_SECONDS = 3
    status = get_stream_status(conn, stream_name)
    while status != 'ACTIVE':
        print('{stream_name} has status: {status}, sleeping for {secs} seconds'.format(
                stream_name = stream_name,
                status      = status,
                secs        = SLEEP_TIME_SECONDS))
        time.sleep(SLEEP_TIME_SECONDS) # sleep for 3 seconds
        status = get_stream_status(conn, stream_name)

def get_stream_status(conn, stream_name):
    '''
    Query this provided connection object for the provided stream's status.

    :type conn: boto.kinesis.layer1.KinesisConnection
    :param conn: A connection to Amazon Kinesis

    :type stream_name: str
    :param stream_name: The name of a stream.

    :rtype: str
    :return: The stream's status
    '''
    description=get_stream_description(conn,stream_name)
    return description.get('StreamStatus')

def get_stream_description(conn,stream_name):
    r= conn.describe_stream(stream_name)
    description = r.get('StreamDescription')
    return description


def get_shards(conn,stream_name):
        description = get_stream_description(conn,stream_name)
        return description.get('Shards')

class PhoneContact(object):
    # __slots__ = ('phoneNumber','primaryName','contactName','contactType','contactOf','belongsTo')
    def __init__(self):
        self.phoneNumber = None
        self.primaryName = None
        self.contactName = None
        self.contactType = None
        self.contactOf = None
        self.belongsTo = None


    def to_json_string(self):
        data = {}
        return json.dumps(self.__dict__)


class UserNode(object):
    # __slots__ = ('appFBId','fullName','legitId','hasContact','belongsTo','fbFriends','legitifiFriends')
    def __init__(self):
        self.appFBId = None
        self.fullName = None
        self.legitId = None
        self.hasContact = []
        self.belongsTo = None
        self.fbFriends = []
        self.legitifiFriends = []
        self.mobiles=None
        self.email=None

    def to_json_string(self):
        data = {}
        return json.dumps(self.__dict__)


def create_user_node(user):
    u = UserNode()
    u.appFBId = user['appFBId']
    u.fullName = user["fullName"]
    u.mobiles=user['mobileNumber']
    u.email=user.get('email',None)
    u.legitId=user.get('legit_id',None)
    return u


def create_phone_contact(u_node,conn):
    contacts = conn.find({'appFBId': u_node.appFBId}, {"_id": 0, "contactRow": 1}, no_cursor_timeout=True)
    contact_nodes=[]
    tc = contacts.count()
    contact_count = 0
    ph = set()
    if contacts.count() > 0:
        for contact in contacts:
            pcontacts = contact["contactRow"]["phones"]
            for phone in pcontacts:
                #pprint.pprint("Creating Contact ",u_node.appFBId)
                if not phone["data"] in ph:
                    contact_count += 1
                    ph.add(phone["data"])
                    pc = PhoneContact()
                    if "fullName" in contact["contactRow"]:
                        pc.contactName = contact["contactRow"]["fullName"]
                    else:
                        pc.contactName = ""
                    pc.phoneNumber = phone["data"]
                    pc.contactType = phone["type"]
                    #print("adding phone contact : ",pc.contactName,pc.phoneNumber," for user : ",u_node.fullName)
                    contact_nodes.append(pc)
    contacts.close()
    return contact_nodes


def get_user_info(userid):

    user = APPUSERS.find_one({"legit_id": userid})
    #print("user is ",user)
    return user



def get_chunks(wholelist,num_of_shards):
    remainder = len(wholelist)%num_of_shards
    num_of_item_in_chunks = int(len(wholelist)/num_of_shards)
    off_set=0
    chunks=[]
    for i in range(0,int((len(wholelist)-remainder)/num_of_item_in_chunks)):
        chunk = wholelist[off_set:off_set+num_of_item_in_chunks]
        off_set=off_set+num_of_item_in_chunks
        chunks.append(chunk)
    if remainder > 0:
        chunks.append(wholelist[-1*remainder:])
    return chunks

def process_records(d,pushtostream='downstream',shardid=None):
    d=json.loads(d)
    downstreamdata = []
    kinesisconn = kinesis.connect_to_region(region_name = region)
    for uid in d:
        u = get_user_info(uid)
        if u:
            u_node = create_user_node(u)
            contacts= create_phone_contact(u_node,APPUSERCONTACTS)
            data = json.dumps(u_node.to_json_string())
            user_contacts=[]
            d={'user':u_node.to_json_string()}
            for cnt in contacts:
               #d = {'user':u_node.to_json_string(),'contact':cnt.to_json_string()}
                user_contacts.append(cnt.to_json_string())
            d['contacts']=user_contacts
            send_to_down_stream(d)
            time.sleep(MONGO_READ_DELAY)
        else:
            print("NO user ",uid)



def read_from_shard(shardid,region,stream_name,iterator_type,kinesis,pushtostream='downstream'):
    import base64
    import json
    shard_it = kinesis.get_shard_iterator(stream_name, shardid,iterator_type)["ShardIterator"]
    last_sequence=None
    last_time_stamp=None
    while shard_it:
        try:
            res = kinesis.get_records(shard_it, limit=10)
            for r in res['Records']:
                #print("Got data in ",shardid)
                last_sequence = r['SequenceNumber']
                d = r['Data']
                process_records(d,pushtostream=pushtostream,shardid=shardid)
                last_sequence = r['SequenceNumber']
                if not last_sequence:
                    print(r)
                last_time_stamp=r.get('ApproximateArrivalTimestamp',None)
            shard_it = res['NextShardIterator']
            time.sleep(5)
        except boto.kinesis.exceptions.ProvisionedThroughputExceededException:
            print('ProvisionedThroughputExceededException found. Sleeping for 2 minutes ','shardid:',shardid,' lastsequence: ',last_sequence,'lasttimestamp: ',last_time_stamp)
            time.sleep(2*60)
            print("Restarting shard : ",shardid," from sequence : ",last_sequence)
            restart=True
            if last_sequence:
                iter_response = kinesis.get_shard_iterator(stream_name=stream_name,shard_id=shardid,shard_iterator_type='AFTER_SEQUENCE_NUMBER',starting_sequence_number=str(last_sequence))
                shard_it = iter_response['ShardIterator']
        except boto.kinesis.exceptions.ExpiredIteratorException:
            if last_sequence:
                iter_response = kinesis.get_shard_iterator(stream_name=stream_name,shard_id=shardid,shard_iterator_type='AFTER_SEQUENCE_NUMBER',starting_sequence_number=str(last_sequence))
                shard_it = iter_response['ShardIterator']
        except Exception as e:
            print("Fatal Error : ",e," shard : ",shardid ,"data : ",d, "sequence : ",last_sequence)
            if last_sequence:
                iter_response = kinesis.get_shard_iterator(stream_name=stream_name,shard_id=shardid,shard_iterator_type='AFTER_SEQUENCE_NUMBER',starting_sequence_number=str(last_sequence))
                shard_it = iter_response['ShardIterator']




def parallel(conn,region,stream_name,shards,iterator_type,pushtostream='downstream'):
    import threading
    for sh in shards:
        print("Thread for shard ",sh['ShardId'])
        threading.Thread(target=read_from_shard,args=(sh['ShardId'],region,stream_name,iterator_type,conn),kwargs={'pushtostream': pushtostream}).start()





def SetUpMongo(mongourl,mongodb,collection):
    global MONGODB,SOCIALDB,APPUSERS,APPUSERCONTACTS
    MONGODB = mongourl
    SOCIALDB = mongodb
    APPUSERS = collection
    SOCIALDB = pymongo.MongoClient(MONGODB)[SOCIALDB]
    APPUSERS = SOCIALDB.get_collection(APPUSERS)
    APPUSERCONTACTS = SOCIALDB.get_collection('appusercontacts')


def InitiateKinesisConnection(stream_name,region):
    '''
    Getting a connection to Amazon Kinesis will require that you have your credentials available to
    one of the standard credentials providers.
    '''
    print("Connecting to stream: {s} in {r}".format(s=stream_name, r=args.region))
    connection = kinesis.connect_to_region(region_name = args.region)
    try:
        status = get_stream_status(connection,stream_name)
        if 'DELETING' == status:
            print('The stream: {s} is being deleted, please rerun the script.'.format(s=stream_name))
            sys.exit(1)
        elif 'ACTIVE' != status:
            wait_for_stream(connection, stream_name)
    except:
        # We'll assume the stream didn't exist so we will try to create it with just one shard
        #conn.create_stream(stream_name, 1)
        wait_for_stream(connection, stream_name)

    return connection


def get_chunks(wholelist,num_of_shards):
    if len(wholelist) < num_of_shards:
        return [wholelist]
    remainder = len(wholelist)%num_of_shards
    num_of_item_in_chunks = int(len(wholelist)/num_of_shards)
    off_set=0
    chunks=[]
    for i in range(0,int((len(wholelist)-remainder)/num_of_item_in_chunks)):
        chunk = wholelist[off_set:off_set+num_of_item_in_chunks]
        off_set=off_set+num_of_item_in_chunks
        chunks.append(chunk)
    if remainder > 0:
        chunks.append(wholelist[-1*remainder:])
    return chunks


def get_down_stream_process_to_send():
    global LAST_DOWNSTREAM_PROCESS
    LAST_DOWNSTREAM_PROCESS=randint(0, len(DOWN_STREAM_PROCESS_MAP)-1)%len(DOWN_STREAM_PROCESS_MAP)
    return LAST_DOWNSTREAM_PROCESS

def send_to_down_stream(data):
    chunks=get_chunks(data,len(DOWN_STREAM_PROCESS_MAP))
    #print("data size: ",len(json.dumps(data))/(1024*1024)," Megabytes")
    LAST_DOWNSTREAM_PROCESS=get_down_stream_process_to_send()
    DOWN_STREAM_PROCESS_MAP[LAST_DOWNSTREAM_PROCESS][1].send(json.dumps(data))


def runprocess(inchannel,region,streamname,shardid):
    try:
        conn = kinesis.connect_to_region(region_name = region)
    except Exception as e:
        print("Could not connect to kinesis region : ",region)
        return
    if not conn:
        print("con is none ",region)
        print(kinesis)
        return
    
    while True:
        done=False
        try:
            data = inchannel.recv()
        except Exception as e:
            print("Socket data error : ",e,"   data : ",data," shardid : ",shardid)
            continue
        if data=='stop':
            return
        while not done:
            done=True
            try:
                d= conn.put_record(streamname, data, shardid)
                print(d)
            except kinesis.exceptions.ProvisionedThroughputExceededException:
                done=False
                print('ProvisionedThroughputExceededException found. Sleeping for 0.5 seconds...')
                time.sleep(0.5)
            except Exception as e:
                done=False
                print(e)

def parallelDownstreamProducers(region,stream_name,shards):
    global DOWN_STREAM_PROCESS_MAP
    processes=[]
    for s in shards:
        parent_conn, child_conn = Pipe()
        p = Process(target=runprocess, args=(child_conn,region,stream_name,s['ShardId']))
        DOWN_STREAM_PROCESS_MAP.append((p,parent_conn))
        p.start()


def set_mongo_delay(delay):
    global MONGO_READ_DELAY
    MONGO_READ_DELAY=int(delay)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--stream", dest="stream_name", required=True,
                      help="The stream you'd like to create.", metavar="STREAM_NAME",)
    parser.add_argument("-r", "--region", "--region", dest="region", default="us-east-1",
                      help="The region you'd like to make this stream in. Default is 'us-east-1'", metavar="REGION_NAME",)
    parser.add_argument("-mongourl", "--mongourl", dest="mongourl", required=True,
                      help="The mongodb to push to kinesis.", metavar="STREAM_NAME",)
    parser.add_argument("-mongodb", "--mongodb", dest="mongodb", required=True,
                      help="The data to push to kinesis.", metavar="STREAM_NAME",)

    parser.add_argument("-collection", "--collection", dest="collection", required=True,
                      help="The data to push to kinesis.", metavar="STREAM_NAME",)
    
    parser.add_argument("-batchsize", "--batchsize", dest="batchsize", required=True,
                      help="Number of records to push to kinesis.", metavar="STREAM_NAME",)
    parser.add_argument("-iter", "--iter", "--iter", dest="iter", default="LATEST",
                      help="The iterator type from stream. Default is 'LATEST'", metavar="REGION_NAME",)
    parser.add_argument("-pushtostream", "--pushtostream", "--pushtostream", dest="pushtostream", default="downstream",
                      help="downstream push", metavar="REGION_NAME",)
    
    parser.add_argument("-mongo_delay", "--mongo_delay", "--mongo_delay", dest="mongo_delay", default=0,
                      help="downstream push", metavar="REGION_NAME",)

           
    args = parser.parse_args()
    stream_name = args.stream_name
    region = args.region
    mongourl = args.mongourl
    mongodb = args.mongodb
    collection = args.collection
    batchsize = int(args.batchsize)
    iter_type=args.iter
    pushtostream=args.pushtostream


    
    set_mongo_delay(args.mongo_delay)

    upconnection=InitiateKinesisConnection(stream_name,region)
    downconnection=InitiateKinesisConnection(pushtostream,region)
    SetUpMongo(mongourl,mongodb,collection)
    shards = get_shards(upconnection,stream_name)
    downshards=get_shards(downconnection,pushtostream)
    parallelDownstreamProducers(region,pushtostream,downshards)
    parallel(upconnection,args.region,args.stream_name,shards,iter_type,pushtostream=pushtostream)
    for i in range(0,len(DOWN_STREAM_PROCESS_MAP)):
        DOWN_STREAM_PROCESS_MAP[i][0].join()
        
