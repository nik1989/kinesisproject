
import pymongo
from neo4j.v1 import GraphDatabase
import json
import sys, random, time, argparse
from bson.objectid import ObjectId






MONGODB = None#'mongodb://10.10.1.60:27017/'
SOCIALDB = None#pymongo.MongoClient(MONGODB)['takenmate']
APPUSERS = None#SOCIALDB.get_collection('appusers')
APPUSERCONTACTS = None#SOCIALDB.get_collection('appusercontacts')



def SetUpMongo(mongourl,mongodb,collection):
    global MONGODB,SOCIALDB,APPUSERS,APPUSERCONTACTS
    MONGODB = mongourl
    SOCIALDB = mongodb
    APPUSERS = collection
    SOCIALDB = pymongo.MongoClient(MONGODB)[SOCIALDB]
    APPUSERS = SOCIALDB.get_collection(APPUSERS)
    APPUSERCONTACTS = SOCIALDB.get_collection('appusercontacts')




def get_usercontacts(batchsize):
    # while True:
    #     yield [1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10]
    firstpage= list(APPUSERCONTACTS.find({}).limit(1).sort("_id",1))
    #firstpage= list(APPUSERCONTACTS.find({"_id":ObjectId("59d6e7d8552f797b6f095258")}).limit(1).sort("_id",1))
    firstuser= firstpage[0] if firstpage else None
    #print firstuser
    if firstuser:
        yield [firstuser]
        last_user = firstuser.get("_id")
        while True:
            nextpage = list(APPUSERCONTACTS.find({"_id":{"$gt":last_user}}).limit(batchsize).sort("_id",1))
            yield [x for x in nextpage]
            if len(nextpage) > 0:
                last_user=nextpage[-1].get("_id")
                print("LAST CONTACT : ",last_user)
            else:
                return

def create_phone_contact_dict(contacts):
    contact_nodes=[]
    ph = set()
    for contact in contacts:
        pcontacts = contact["contactRow"]["phones"]
        for phone in pcontacts:
            #pprint.pprint("Creating Contact ",u_node.appFBId)
            if not phone["data"] in ph:
                if len(phone["data"]) >= 10:
                    ph.add(phone["data"])
                    pc = dict()
                    if "fullName" in contact["contactRow"]:
                        pc['contactName'] = contact["contactRow"]["fullName"]
                    else:
                        pc['contactName'] = ""
                    pc['phoneNumber'] = phone["data"]
                    pc['contactType'] = phone["type"]
                    pc['appFBId']=contact['appFBId']
                    with driver.session() as session:
                        with session.begin_transaction() as tx:
                            tx.run("Create (u:PhoneContact {contactName:{contactName},phoneNumber:{phoneNumber},contactType:{contactType},appFBId:{appFBId}});",**pc)
                    #print("adding phone contact : ",pc.contactName,pc.phoneNumber," for user : ",u_node.fullName)
                    





if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-mongourl", "--mongourl", dest="mongourl",
                      help="The mongodb to push to kinesis.", metavar="STREAM_NAME",default="mongodb://10.0.3.234:27017/",)
    parser.add_argument("-mongodb", "--mongodb", dest="mongodb", 
                      help="The data to push to kinesis.", metavar="STREAM_NAME",default="takenmate",)

    parser.add_argument("-collection", "--collection", dest="collection", 
                      help="The data to push to kinesis.", metavar="STREAM_NAME",default="appusers",)
    
    parser.add_argument("-batchsize", "--batchsize", dest="batchsize", 
                      help="Number of records to push to kinesis.", metavar="STREAM_NAME",default=200,)

    parser.add_argument("-neo4jhost", "--neo4jhost", "--neo4jhost", dest="neohost", default="10.0.3.85:7474",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    parser.add_argument("-neo4jdb", "--neo4jdb", "--neo4jdb", dest="neodb", default="data/db",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    parser.add_argument("-neo4juser", "--neo4juser", "--neo4juser", dest="neo4juser", default="neo4j",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)
                    
    parser.add_argument("-neo4jpass", "--neo4jpass", "--neo4jpass", dest="neo4jpass", default="neo4j!123",
                      help="The neo4j host to connect", metavar="NEO_HOST_NAME",)

    try:      
        args = parser.parse_args()
    except Exception as e:
        print(e)

    mongourl = args.mongourl
    mongodb = args.mongodb
    collection = args.collection
    batchsize = int(args.batchsize)

    #setBatch(batchsize)

    neo4jhost=args.neohost
    neo4jdb=args.neodb
    neo4juser=args.neo4juser
    neo4jpass=args.neo4jpass



    SetUpMongo(mongourl,mongodb,collection)
    uri = "bolt://localhost:7687"
    driver = GraphDatabase.driver(uri, auth=("neo4j", "neodev"))
    for contacts in get_usercontacts(200):
        create_phone_contact_dict(contacts)


