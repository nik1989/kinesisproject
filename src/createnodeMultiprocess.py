
import pymongo
from py2neo.ogm import GraphObject, Property, Related, RelatedTo
from py2neo import Graph, Relationship, authenticate
from bson.objectid import ObjectId
import json
# from PushToKinesis import execute
import os
from multiprocessing import Process
from boto import kinesis

ENVIRON = os.environ

REGION = ENVIRON.get('REGION','us-east-2')
DOWNSTREAM = ENVIRON.get('DOWNSTREAM','downstream')
MONGODB = ENVIRON.get('MONGOHOST','mongodb://18.217.19.149:27017/')
SOCIALDB = ENVIRON.get('MONGODB','socialgraph')
APPUSERS = ENVIRON.get('USERCOLLECTOIN','appusers')
APPUSERCONTACTS = ENVIRON.get('CONTACTSCOLLECTOIN','appusercontacts')


# SOCIALDB = pymongo.MongoClient(MONGODB)[SOCIALDB]
# APPUSERS = SOCIALDB.get_collection(APPUSERS)
# APPUSERCONTACTS = SOCIALDB.get_collection(APPUSERCONTACTS)

# authenticate("18.217.19.149:7474", "neo4j", "nik@neo4j")
# NEO = 'http://18.217.19.149:7474/data/db'
# neograph = Graph(NEO, bolt=False)


# testIds = ["10154944004806305", ]
def wait_for_stream(conn, stream_name):
    '''
    Wait for the provided stream to become active.

    :type conn: boto.kinesis.layer1.KinesisConnection
    :param conn: A connection to Amazon Kinesis

    :type stream_name: str
    :param stream_name: The name of a stream.
    '''
    SLEEP_TIME_SECONDS = 3
    status = get_stream_status(conn, stream_name)
    while status != 'ACTIVE':
        print('{stream_name} has status: {status}, sleeping for {secs} seconds'.format(
                stream_name = stream_name,
                status      = status,
                secs        = SLEEP_TIME_SECONDS))
        time.sleep(SLEEP_TIME_SECONDS) # sleep for 3 seconds
        status = get_stream_status(conn, stream_name)

def get_stream_status(conn, stream_name):
    '''
    Query this provided connection object for the provided stream's status.

    :type conn: boto.kinesis.layer1.KinesisConnection
    :param conn: A connection to Amazon Kinesis

    :type stream_name: str
    :param stream_name: The name of a stream.

    :rtype: str
    :return: The stream's status
    '''
    description=get_stream_description(conn,stream_name)
    return description.get('StreamStatus')

def get_stream_description(conn,stream_name):
    r= conn.describe_stream(stream_name)
    description = r.get('StreamDescription')
    return description


def get_shards(conn,stream_name):
        description = get_stream_description(conn,stream_name)
        return description.get('Shards')

class PhoneContact(object):
    # __slots__ = ('phoneNumber','primaryName','contactName','contactType','contactOf','belongsTo')
    def __init__(self):
        self.phoneNumber = None
        self.primaryName = None
        self.contactName = None
        self.contactType = None
        self.contactOf = None
        self.belongsTo = None


    def to_json_string(self):
        data = {}
        return json.dumps(self.__dict__)


class UserNode(object):
    # __slots__ = ('appFBId','fullName','legitId','hasContact','belongsTo','fbFriends','legitifiFriends')
    def __init__(self):
        self.appFBId = None
        self.fullName = None
        self.legitId = None
        self.hasContact = []
        self.belongsTo = None
        self.fbFriends = []
        self.legitifiFriends = []
        self.mobiles=None

    def to_json_string(self):
        data = {}
        for k,v in self.__dict__.items():
            if type(v)==type([]):
                #print k
                ldata = []
                for x in v:
                    ldata.append(x.to_json_string())
                data[k]=ldata
            elif hasattr(v,'to_json_string'):
                data[k]=v.to_json_string()
            else:
                data[k]=v
        return data


def create_user_node(user):
    u = UserNode()
    u.appFBId = user['appFBId']
    u.fullName = user["contactRow"]["fullName"]
    u.mobiles=user['mobileNumber']
    return u


def create_phone_contact(u_node,conn):
    contacts = conn.find({'appFBId': u_node.appFBId}, {"_id": 0, "contactRow": 1}, no_cursor_timeout=True).batch_size(2000)
    contact_nodes=[]
    tc = contacts.count()
    contact_count = 0
    ph = set()
    if contacts.count() > 0:
        for contact in contacts:
            pcontacts = contact["contactRow"]["phones"]
            for phone in pcontacts:
                #pprint.pprint("Creating Contact ",u_node.appFBId)
                if not phone["data"] in ph:
                    contact_count += 1
                    ph.add(phone["data"])
                    pc = PhoneContact()
                    if "fullName" in contact["contactRow"]:
                        pc.contactName = contact["contactRow"]["fullName"]
                    else:
                        pc.contactName = ""
                    pc.phoneNumber = phone["data"]
                    pc.contactType = phone["type"]
                    u_node.hasContact.append(pc)
    contacts.close()
    return u_node


def get_user_info(userid,conn):
    print(userid)
    user = conn.find_one({"appFBId": userid})
    return user



def get_chunks(wholelist,num_of_shards):
    remainder = len(wholelist)%num_of_shards
    num_of_item_in_chunks = int(len(wholelist)/num_of_shards)
    off_set=0
    chunks=[]
    for i in range(0,int((len(wholelist)-remainder)/num_of_item_in_chunks)):
        chunk = wholelist[off_set:off_set+num_of_item_in_chunks]
        off_set=off_set+num_of_item_in_chunks
        chunks.append(chunk)
    if remainder > 0:
        chunks.append(wholelist[-1*remainder:])
    return chunks

def process_records(encodeddata,region,pushtostream,shardid):
    import base64
    import json
    d = base64.b64decode(encodeddata).decode('utf-8')
    d=json.loads(d)
    downstreamdata = []
    kinesisconn = kinesis.connect_to_region(region_name = region)
    SOCIALDBCONN = pymongo.MongoClient(MONGODB)[SOCIALDB]
    APPUSERSCONN = SOCIALDBCONN.get_collection(APPUSERS)
    APPUSERCONTACTSCONN = SOCIALDBCONN.get_collection(APPUSERCONTACTS)
    for uid in d["ids"]:
        u = get_user_info(uid,APPUSERSCONN)
        if u:
            u_node = create_user_node(u)
            u_node= create_phone_contact(u_node,APPUSERCONTACTSCONN)
            contacts = u_node.hasContact
            u_node.hasContact=[]
            user_contacts=[]
            d={'user':u_node.to_json_string()}
            for cnt in contacts:
                user_contacts.append(cnt.to_json_string())
            d['contacts']=user_contacts
            done=False
            while not done:
                try:
                    d= kinesisconn.put_record(pushtostream, json.dumps(d), shardid)
                    done=True
                except kinesis.exceptions.ProvisionedThroughputExceededException:
                    done=False
                    print('ProvisionedThroughputExceededException found. Sleeping for 0.5 seconds...')
                    time.sleep(0.5)
                except Exception as e:
                    done=False
                    print(e)
        else:
            print("NO user ",uid)


def multi_lambda_handler(event, context):
    # TODO implement
    import random
    from boto import kinesis
    processes=[]
    last_shard=None
    kinesisconn = kinesis.connect_to_region(region_name = REGION)
    try:
        status = get_stream_status(kinesisconn,DOWNSTREAM)
        if 'DELETING' == status:
            print('The stream: {s} is being deleted, please rerun the script.'.format(s=DOWNSTREAM))
            sys.exit(1)
        elif 'ACTIVE' != status:
            wait_for_stream(kinesisconn, DOWNSTREAM)
    except:
        # We'll assume the stream didn't exist so we will try to create it with just one shard
        #conn.create_stream(stream_name, 1)
        wait_for_stream(kinesisconn, DOWNSTREAM)
    shards=[x['ShardId'] for x  in get_shards(kinesisconn,DOWNSTREAM)]
    for r in event['Records']:
        d = r['kinesis']['data']
        sid=shards[random.randint(0,len(shards))%len(shards)]
        process = Process(target=process_records, args=(d,REGION,DOWNSTREAM,sid,))
        processes.append(process)
    for process in processes:
        process.start()
    for process in processes:
        process.join()



def lambda_handler(event, context):
    # TODO implement
    import base64
    import json
    for r in event['Records']:
        d = r['kinesis']['data']
        d = base64.b64decode(d).decode('utf-8')
        d=json.loads(d)
        downstreamdata = []
        for uid in d['ids']:
            u = get_user_info(uid)
            u_node = create_user_node(u)
            u_node= create_phone_contact(u_node)
            data = json.dumps(u_node.to_json_string())
            contacts = u_node.hasContact
            u_node.hasContact=[]
            for cnt in contacts:
                d = {'user':u_node.to_json_string(),'contact':cnt.to_json_string()}
                print(d)
                downstreamdata.append(d)
        #execute('python','AppPhoneContactsProducer.py',REGION,DOWNSTREAM,downstreamdata)
        
