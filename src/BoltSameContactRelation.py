
import pymongo
from neo4j.v1 import GraphDatabase
import json
import sys, random, time, argparse
from bson.objectid import ObjectId




def get_user_phone_contact_node(driver):
    with driver.session() as session:
        with session.begin_transaction() as tx:
             for rec in tx.run("MATCH (n:UserNode) RETURN n.appFBId,n.fullName,n.mobile"):
                 yield rec


def Create_same_Contact(rec,driver):
    with driver.session() as session:
        with session.begin_transaction() as tx:
             if len(rec['n.mobile']) > 1:
                data = {'appFBId':rec['n.appFBId']}
                tx.run("MATCH (n:UserNode{appFBId:{appFBId}})-[:phone_contacts]->(p:PhoneContact) with p match(p2:PhoneContact{phoneNumber:p.phoneNumber})  where not p=p2 merge (p)-[:same_contact]-(p2)",**data)


if __name__ == '__main__':
    uri = "bolt://10.0.3.94:7687"
    driver = GraphDatabase.driver(uri, auth=("neo4j", "neo4j!123"))
    count=0
    for rec in get_user_phone_contact_node(driver):
        count=count+1
        Create_same_Contact(rec,driver)
        print(count)